﻿using System;

public class User
{
    public int id { get; set; }
    public string surname { get; set; }
    public string name { get; set; }
    public string patronymic { get; set; }
    public string phone_number { get; set; }
    public string country { get; set; }
    public DateTime date_birth { get; set; }
    public string company { get; set; }
    public string position { get; set; }
    public string other { get; set; }
}