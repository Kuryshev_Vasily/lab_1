﻿using System;
using System.Collections.Generic;


namespace NotebookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> list_data = new List<User>();
            NoteBook operation = new NoteBook();

            while (true)
            {
                Console.WriteLine("Список команд");
                Console.WriteLine("1 - Добавить новую запись");
                Console.WriteLine("2 - Редактирование записи");
                Console.WriteLine("3 - Удаление записи");
                Console.WriteLine("4 - Просмотр одной записис");
                Console.WriteLine("5 - Просмотр всех записей");
                Console.WriteLine("6 - Остановить программу");
                Console.Write("\nВведите команду: ");
                int var = Convert.ToInt32(Console.ReadLine());
                if (var == 6)
                {
                    break;
                }
                
                switch (var)
                {
                    case 1:
                        list_data.Add(operation.create_entry());
                        break;
                    case 2:
                        list_data = operation.edit_entry(list_data);
                        break;
                    case 3:
                        Console.Clear();
                        list_data = operation.remove_user(list_data);
                        break;
                    case 4:
                        operation.detail_data(list_data);
                        break;
                    case 5:
                        Console.WriteLine("Список контактов:");
                        Console.Clear();
                        operation.show_all(list_data);
                        break;
                }
            }
        }
    }
}
