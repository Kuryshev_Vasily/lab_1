﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;


public class NoteBook
{
    private static int count_entry = 1;

    private class ValidatorData
    {
        public string check_str(string message)
        {
            Console.WriteLine(message);
            while (true)
            {
                string value = Console.ReadLine();
                if (Regex.IsMatch(value, @"^[a-zA-Zа-яА-Я\- ]+"))
                {
                    return value;
                }
                Console.WriteLine("Введенная вами строка не корректна, " +
                    "Вы можете использовать только русские и латинские буквы, " +
                    "а также пробел и -");
            }
        }

        public string check_phone(string message)
        {
            Console.WriteLine(message);
            while (true)
            {
                string value = Console.ReadLine().Replace(" ", "");
                if (Regex.IsMatch(value, @"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"))
                {
                    return value;
                }
                Console.WriteLine("Введенный вами номер телефона некорректен. Повторите ввод");
            }
        }

        public DateTime check_date(string message)
        {
            Console.WriteLine(message);
            while (true)
            {
                string[] formats = {
                    "d/M/yyyy", "d.M.yyyy",
                    "d/MM/yyy", "d.MM.yyyy",
                    "dd/MM/yyyy", "dd.MM.yyyy",
                    "dd-MM-yyyy", "d-M-yyyy",
                    "d.M.yy", "d/M/yy",
                };

                string dateString = Console.ReadLine().Replace(" ", "");
                DateTime dateValue;

                if (DateTime.TryParseExact(
                                            dateString,
                                            formats,
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.None,
                                            out dateValue
                                            ))
                {
                    return dateValue;
                }
                Console.WriteLine("Введенная дата не корректна. Повторите ввод.");
            }
        }
    }

    private struct UsrStruct
    {
        public string surname;
        public string name;
        public string phone_number;
        public DateTime date_birth;
        public string patronymic;
        public string country;
        public string company;
        public string position;
        public string other;
    }

    private User save(UsrStruct data)
    {
        User user = new User();
        user.id           = NoteBook.count_entry;
        user.surname      = data.surname;
        user.name         = data.name;
        user.patronymic   = data.patronymic;
        user.phone_number = data.phone_number;
        user.country      = data.country;
        user.date_birth   = data.date_birth;
        user.company      = data.company;
        user.position     = data.position;
        user.other        = data.position;

        NoteBook.count_entry++;
        return user;
    }

    private UsrStruct parse_data()
    {

        UsrStruct data = new UsrStruct();
        ValidatorData validator = new ValidatorData();
        Console.Clear();
        Console.WriteLine("Введите обязательные поля: ");
        data.surname      = validator.check_str("Введите фамилию человека");
        data.name         = validator.check_str("Введите имя человека");
        data.phone_number = validator.check_phone("Введите номер телефона (Например +7(911) 232-3333)");

        while (true)
        {
            Console.WriteLine("Хотите ввести дополнительные сведения y/n: ");
            string resp = Console.ReadLine();
            if (resp.Equals("y"))
            {
                data.patronymic = validator.check_str("Введите отчество пользователя");
                data.date_birth = validator.check_date("Введите дату:");
                data.country    = validator.check_str("Введите страну:");
                data.company    = validator.check_str("Введите компанию:");
                data.position   = validator.check_str("Введите должность:");
                data.other      = validator.check_str("Введите другие сведения:");
                break;
            }
            if (resp.Equals("n"))
            {
                break;
            }
        }
        Console.Clear();
        return data;
    }

    public User create_entry()
    {
        User user = this.save(this.parse_data());
        Console.WriteLine($"Контакт {user.surname} {user.name} создан!\n");
        return user;
    }

    private User get_entry(List<User> data, string message)
    {
        Console.WriteLine("Список контактов:");
        this.show_all(data);
        Console.WriteLine(message);
        while (true)
        {
            try
            {
                int number = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                User usr = data.Find(x => x.id == number);
                if (usr.id.Equals(number))
                {
                    return usr;
                }
                Console.WriteLine("Введенный номер не корректен, введите снова.");
            }
            catch
            {
                Console.WriteLine("Введенный номер не корректен, введите снова.");
                continue;
            }

        }
    }

    public List<User> edit_entry(List<User> data)
    {
        Console.Clear();
        if (data.Count != 0)
        {
            User usr = this.get_entry(data, "Введите номер записи, которую нужно отредактировать:");
            ValidatorData validator = new ValidatorData();

            while (true)
            {
                Console.WriteLine("Текущие данные записи:");
                this.show_one_entry(usr);
                Console.WriteLine("Список команд:");
                Console.WriteLine("1 - Редактировать фамилию контакта");
                Console.WriteLine("2 - Редактировать имя контакта");
                Console.WriteLine("3 - Редактировать дату рождения контакта");
                Console.WriteLine("4 - Редактировать Отчество контакта");
                Console.WriteLine("5 - Редактировать номер телефона контакта");
                Console.WriteLine("6 - Редактировать страну контакта");
                Console.WriteLine("7 - Редактировать компанию контакта");
                Console.WriteLine("8 - Редактировать должность контакта");
                Console.WriteLine("9 - Редактировать другие сведения контакта");
                Console.WriteLine("10 - Сохранить и завершить редактирование");
                Console.Write("\nВедите номер команды: ");
                try
                {
                    int number = Convert.ToInt32(Console.ReadLine());

                    if (number.Equals(10))
                    {
                        Console.Clear();
                        break;
                    }

                    switch (number)
                    {
                        case 1:
                            usr.surname = validator.check_str("Введите фамилию человека");
                            break;
                        case 2:
                            usr.name = validator.check_str("Введите имя человека");
                            break;
                        case 3:
                            usr.date_birth = validator.check_date("Введите дату:");
                            break;
                        case 4:
                            usr.patronymic = validator.check_str("Введите отчество пользователя");
                            break;
                        case 5:
                            usr.phone_number = validator.check_phone("Введите номер телефона (Пример +7(911) 232-3333)");
                            break;
                        case 6:
                            usr.country = validator.check_str("Введите страну:");
                            break;
                        case 7:
                            usr.company = validator.check_str("Введите компанию:");
                            break;
                        case 8:
                            usr.position = validator.check_str("Введите должность:");
                            break;
                        case 9:
                            usr.other = validator.check_str("Введите другие сведения:");
                            break;
                    }
                }
                catch
                {
                    Console.Clear();
                    Console.WriteLine("Введенный номер не корректен или не совпадает, введите снова.");
                    continue;
                }
            }
            foreach (User elm in data)
            {
                if (elm.id.Equals(usr.id))
                {
                    elm.surname = usr.surname;
                    elm.name = usr.name;
                    elm.patronymic = usr.patronymic;
                    elm.phone_number = usr.phone_number;
                    elm.date_birth = usr.date_birth;
                    elm.country = usr.country;
                    elm.company = usr.company;
                    elm.position = usr.position;
                    elm.other = usr.other;

                    break;
                }
            }
        }
        return data;            
    }


    public List<User> remove_user(List<User> users)
    {
        Console.Clear();
        if (users.Count != 0)
        {
            Console.Clear();
            User usr = this.get_entry(users, "Выберете номер записи для удаления:");
            Console.WriteLine($"Пользователь {usr.surname} {usr.name} удален.");
            users.Remove(usr);
        } 
        return users;
    }

    public void detail_data(List<User> users)
    {
        Console.Clear();
        if (users.Count != 0)
        {
            User usr = this.get_entry(users, "Выберете номер записи для подробного просмотра:");
            Console.Clear();
            this.show_one_entry(usr);
        }
    }

    public void show_all(List<User> data)
    {
        if (data.Count != 0)
        {
            foreach (User elm in data)
            {
                Console.WriteLine($"{elm.id} {elm.surname} {elm.name}");
            }
            Console.WriteLine();
        }
    }

    private void show_one_entry(User user)
    {
        Console.WriteLine($"Фамилия: {user.surname}");
        Console.WriteLine($"Имя: {user.name}");
        Console.WriteLine($"Отчество: {user.patronymic}");
        Console.WriteLine($"Номер телефона: {user.phone_number}");
        Console.WriteLine($"Страна: {user.country}");
        Console.WriteLine($"Дата рожления: {user.date_birth.ToString("dd.MM.yyyy")}");
        Console.WriteLine($"Название компании: {user.company}");
        Console.WriteLine($"Должность: {user.position}");
        Console.WriteLine($"Другие сведения: {user.other}");
        Console.WriteLine();
    }
}